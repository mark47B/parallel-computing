#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include "mpi.h"

#define MATRIX_SIZE 512

void printMatrix(int matrix[MATRIX_SIZE][MATRIX_SIZE]);
bool checkResult(int A[MATRIX_SIZE][MATRIX_SIZE], int B[MATRIX_SIZE][MATRIX_SIZE], int C[MATRIX_SIZE][MATRIX_SIZE]);

int main(int argc, char **argv)
{
    int process_size, process_rank;
    MPI_Status status;
    int tasks, rows, offset, source, destination;
    double start_time, end_time;
    int C[MATRIX_SIZE][MATRIX_SIZE], A[MATRIX_SIZE][MATRIX_SIZE], B[MATRIX_SIZE][MATRIX_SIZE];

    srand(time(NULL));

    for (int i = 0; i < MATRIX_SIZE; ++i)
        for (int j = 0; j < MATRIX_SIZE; ++j)
        {
            A[i][j] = rand() % 20 - 10;
            B[i][j] = rand() % 20 - 10;
        }

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &process_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &process_size);

    MPI_Barrier(MPI_COMM_WORLD);

    if (process_rank == 0)
    {
        start_time = MPI_Wtime();

        rows = MATRIX_SIZE / process_size;
        offset = rows;

        for (destination = 1; destination < process_size; ++destination)
        {
            MPI_Send(&offset, 1, MPI_INT, destination, 1, MPI_COMM_WORLD);
            MPI_Send(&rows, 1, MPI_INT, destination, 1, MPI_COMM_WORLD);
            MPI_Send(&A[offset][0], rows * MATRIX_SIZE, MPI_INT, destination, 1, MPI_COMM_WORLD);
            MPI_Send(&B, MATRIX_SIZE * MATRIX_SIZE, MPI_INT, destination, 1, MPI_COMM_WORLD);

            offset = offset + rows;
        }

        for (int k = 0; k < MATRIX_SIZE; ++k)
            for (int i = 0; i < rows; ++i)
            {
                C[i][k] = 0;
                for (int j = 0; j < MATRIX_SIZE; ++j)
                    C[i][k] = C[i][k] + A[i][j] * B[j][k];
            }

        for (source = 1; source < process_size; ++source)
        {
            MPI_Recv(&offset, 1, MPI_INT, source, 2, MPI_COMM_WORLD, &status);
            MPI_Recv(&rows, 1, MPI_INT, source, 2, MPI_COMM_WORLD, &status);
            MPI_Recv(&C[offset][0], rows * MATRIX_SIZE, MPI_INT, source, 2, MPI_COMM_WORLD, &status);
        }

        end_time = MPI_Wtime();

        printf("Matrix Multiplication: A*B=C\n");

        // -------------- Uncomment to output matrices --------------

        // printf("Matrix A\n");
        // printMatrix(A);

        // printf("\nMatrix B\n");
        // printMatrix(B);

        // printf("\nResult matrix C\n");
        // printMatrix(C);

        printf("\nTime(ms): %f \n", (end_time - start_time) * 1000);

        if (checkResult(A, B, C))
        {
            printf("Correct result\n");
        }
        else
        {
            printf("Incorrect result\n");
        }
    }
    else
    {
        source = 0;

        MPI_Recv(&offset, 1, MPI_INT, source, 1, MPI_COMM_WORLD, &status);
        MPI_Recv(&rows, 1, MPI_INT, source, 1, MPI_COMM_WORLD, &status);
        MPI_Recv(&A, rows * MATRIX_SIZE, MPI_INT, source, 1, MPI_COMM_WORLD, &status);
        MPI_Recv(&B, MATRIX_SIZE * MATRIX_SIZE, MPI_INT, source, 1, MPI_COMM_WORLD, &status);

        for (int k = 0; k < MATRIX_SIZE; ++k)
            for (int i = 0; i < rows; ++i)
            {
                C[i][k] = 0;
                for (int j = 0; j < MATRIX_SIZE; ++j)
                    C[i][k] = C[i][k] + A[i][j] * B[j][k];
            }

        MPI_Send(&offset, 1, MPI_INT, source, 2, MPI_COMM_WORLD);
        MPI_Send(&rows, 1, MPI_INT, source, 2, MPI_COMM_WORLD);
        MPI_Send(&C, rows * MATRIX_SIZE, MPI_INT, source, 2, MPI_COMM_WORLD);
    }

    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Finalize();

    return 0;
}

void printMatrix(int matrix[MATRIX_SIZE][MATRIX_SIZE])
{
    int i, j;

    for (i = 0; i < MATRIX_SIZE; ++i)
    {
        for (j = 0; j < MATRIX_SIZE; ++j)
            printf("%d ", matrix[i][j]);
        printf("\n");
    }
    printf("\n");
}

bool checkResult(int A[MATRIX_SIZE][MATRIX_SIZE], int B[MATRIX_SIZE][MATRIX_SIZE], int C[MATRIX_SIZE][MATRIX_SIZE])
{
    int i, j, k, D[MATRIX_SIZE][MATRIX_SIZE];

    for (i = 0; i < MATRIX_SIZE; ++i)
        for (j = 0; j < MATRIX_SIZE; ++j)
        {
            D[i][j] = 0;

            for (k = 0; k < MATRIX_SIZE; ++k)
            {
                D[i][j] += A[i][k] * B[k][j];
            }

            if (D[i][j] != C[i][j])
            {
                return false;
            }
        }

    return true;
}
