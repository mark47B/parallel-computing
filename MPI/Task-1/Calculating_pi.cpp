#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"

#define PI 3.14159265358979323846

int main(int argc, char **argv)
{
    int process_rank, process_size;
    int i;
    double local_pi = .0, global_pi = .0, x;
    const int precision = 1e6;

    MPI_Init(&argc, &argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &process_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &process_size);

    MPI_Barrier(MPI_COMM_WORLD);
    double start_time = MPI_Wtime();

    for (i = process_rank; i < precision; i += process_size)
    {
        x = (i + 0.5) / precision;
        local_pi += 4 / (1 + x * x);
    }

    MPI_Reduce(&local_pi, &global_pi, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

    MPI_Barrier(MPI_COMM_WORLD);
    double end_time = MPI_Wtime();

    if (process_rank == 0)
    {
        global_pi /= precision;

        printf("Precision: %u \n", precision);
        printf("PI: %9.8f \n", PI);
        printf("Calculated: %9.8f \n", global_pi);
        printf("Difference: %e \n", PI - global_pi);
        printf("Time(ms): %f \n", (end_time - start_time) * 1000);
    }

    MPI_Finalize();
    return 0;
}
