#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include "mpi.h"

#define N 2048

void merge(int *arr, int left, int mid, int right);
void mergeSort(int *arr, int left, int right);
bool checkSort(int *arr, int size);
void printArray(int *arr, int size);

int main(int argc, char **argv)
{
    int process_size, process_rank, i, sub_size;
    double start_time, end_time;
    int *sub_array;
    int *sorted_array;
    int *array = (int *)malloc(N * sizeof(int));

    srand(time(NULL));
    for (i = 0; i < N; ++i)
    {
        array[i] = rand() % 200 - 100;
    }

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &process_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &process_rank);

    sub_size = N / process_size;

    MPI_Barrier(MPI_COMM_WORLD);
    start_time = MPI_Wtime();

    sub_array = (int *)malloc(sub_size * sizeof(int));
    MPI_Scatter(array, sub_size, MPI_INT, sub_array, sub_size, MPI_INT, 0, MPI_COMM_WORLD);

    mergeSort(sub_array, 0, sub_size - 1);

    if (process_rank == 0)
    {
        sorted_array = (int *)malloc(N * sizeof(int));
    }

    MPI_Gather(sub_array, sub_size, MPI_INT, sorted_array, sub_size, MPI_INT, 0, MPI_COMM_WORLD);

    if (process_rank == 0)
    {
        mergeSort(sorted_array, 0, N - 1);
        end_time = MPI_Wtime();

        printf("Merge Sort\n");

        printf("Original Array:\n");
        printArray(array, N);

        printf("Sorted Array:\n");
        printArray(sorted_array, N);

        if (checkSort(sorted_array, N))
        {
            printf("Correct result\n");
            printf("\nTime(ms): %f \n", (end_time - start_time) * 1000);
        }
        else
        {
            printf("Incorrect result\n");
        }

        free(sorted_array);
    }

    free(array);
    free(sub_array);

    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Finalize();

    return 0;
}

void merge(int *arr, int left, int mid, int right)
{
    int i, j, k;
    int n1 = mid - left + 1;
    int n2 = right - mid;

    int *left_array = (int *)malloc(n1 * sizeof(int));
    int *right_array = (int *)malloc(n2 * sizeof(int));

    for (i = 0; i < n1; i++)
        left_array[i] = arr[left + i];

    for (j = 0; j < n2; j++)
        right_array[j] = arr[mid + 1 + j];

    i = 0;
    j = 0;
    k = left;

    while (i < n1 && j < n2)
    {
        if (left_array[i] <= right_array[j])
        {
            arr[k] = left_array[i];
            i++;
        }
        else
        {
            arr[k] = right_array[j];
            j++;
        }
        k++;
    }

    while (i < n1)
    {
        arr[k] = left_array[i];
        i++;
        k++;
    }

    while (j < n2)
    {
        arr[k] = right_array[j];
        j++;
        k++;
    }

    free(left_array);
    free(right_array);
}

void mergeSort(int *arr, int left, int right)
{
    if (left >= right)
        return;

    int mid = left + (right - left) / 2;
    mergeSort(arr, left, mid);
    mergeSort(arr, mid + 1, right);
    merge(arr, left, mid, right);
}

bool checkSort(int *arr, int size)
{
    for (int i = 0; i < size - 1; i++)
        if (arr[i] > arr[i + 1])
            return false;

    return true;
}

void printArray(int *arr, int size)
{
    int i;

    for (i = 0; i < size; ++i)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
}
