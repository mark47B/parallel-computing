#include <iostream>
#include <random>
#include <omp.h>

void merge(intptr_t* array, const intptr_t left, const intptr_t middle, const intptr_t right) {
    const intptr_t size_left = middle - left + 1;
    const intptr_t size_right = right - middle;

    intptr_t* left_array = new intptr_t[size_left];
    intptr_t* right_array = new intptr_t[size_right];

    for (intptr_t i = 0; i < size_left; i++) {
        left_array[i] = array[left + i];
    }

    for (intptr_t j = 0; j < size_right; j++) {
        right_array[j] = array[middle + 1 + j];
    }

    intptr_t i = 0, j = 0, k = left;

    while (i < size_left && j < size_right) {
        if (left_array[i] <= right_array[j]) {
            array[k] = left_array[i];
            i++;
        }
        else {
            array[k] = right_array[j];
            j++;
        }
        k++;
    }

    while (i < size_left) {
        array[k] = left_array[i];
        i++;
        k++;
    }

    while (j < size_right) {
        array[k] = right_array[j];
        j++;
        k++;
    }

    delete[] left_array;
    delete[] right_array;
}

void mergeSort(intptr_t* array, const intptr_t left, const intptr_t right) {
    if (left >= right)
        return;

    intptr_t middle = left + (right - left) / 2;
    mergeSort(array, left, middle);
    mergeSort(array, middle + 1, right);
    merge(array, left, middle, right);
}

void parallelMergeSort(intptr_t* array, const intptr_t left, const intptr_t right) {
    if (left >= right)
        return;

    intptr_t middle = left + (right - left) / 2;

#pragma omp task
    parallelMergeSort(array, left, middle);
#pragma omp task
    parallelMergeSort(array, middle + 1, right);

    merge(array, left, middle, right);
}

int main() {
    std::cout << "Merge Sort" << std::endl;

    const intptr_t size = 1e8;
    const intptr_t min_value = -100;
    const intptr_t max_value = 100;
    srand(time(NULL));

    intptr_t* array1 = new intptr_t[size];
    intptr_t* array2 = new intptr_t[size];
    for (intptr_t i = 0; i < size; ++i) {
        array1[i] = min_value + rand() % (max_value - min_value + 1);
        array2[i] = array1[i];
    }

    std::cout << "Sequential" << std::endl;
    double start1 = omp_get_wtime();
    mergeSort(array1, 0, size - 1);
    double end1 = omp_get_wtime();
    std::cout << "Time in ms: " << (end1 - start1) * 1000 << std::endl;

    std::cout << "Parallel" << std::endl;
    double start2 = omp_get_wtime();
#pragma omp parallel
    {
#pragma omp single
        parallelMergeSort(array2, 0, size - 1);
    }
    
    double end2 = omp_get_wtime();
    std::cout << "Time in ms: " << (end2 - start2) * 1000 << std::endl;

    delete[] array1;
    delete[] array2;

    return 0;
}