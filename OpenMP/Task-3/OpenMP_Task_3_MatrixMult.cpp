#include <iostream>
#include <random>
#include <omp.h>

int main() {
    std::cout << "Matrix Multiplication" << std::endl;

    const intptr_t MIN_VALUE = -100;
    const intptr_t MAX_VALUE = 100;
    srand(time(NULL));

    intptr_t matrix_size = 1e3;
    intptr_t** matrixA = new intptr_t * [matrix_size];
    intptr_t** matrixB = new intptr_t * [matrix_size];
    intptr_t** matrixC = new intptr_t * [matrix_size];

    for (intptr_t i = 0; i < matrix_size; ++i) {
        matrixA[i] = new intptr_t[matrix_size];
        matrixB[i] = new intptr_t[matrix_size];
        matrixC[i] = new intptr_t[matrix_size];
    }

    for (intptr_t i = 0; i < matrix_size; ++i)
        for (intptr_t j = 0; j < matrix_size; ++j) {
            matrixA[i][j] = MIN_VALUE + rand() % (MAX_VALUE - MIN_VALUE + 1);
            matrixB[i][j] = MIN_VALUE + rand() % (MAX_VALUE - MIN_VALUE + 1);
            matrixC[i][j] = 0;
        }

    double start_time = omp_get_wtime();
#pragma omp parallel for num_threads(8)
    for (int i = 0; i < matrix_size; ++i) {
        for (int j = 0; j < matrix_size; ++j) {
            for (int k = 0; k < matrix_size; ++k) {
                matrixC[i][j] += matrixA[i][k] * matrixB[k][j];
            }
        }
    }
    double end_time = omp_get_wtime();

    std::cout << "Time in ms: " << (end_time - start_time) * 1000 << std::endl;

    return 0;
}
